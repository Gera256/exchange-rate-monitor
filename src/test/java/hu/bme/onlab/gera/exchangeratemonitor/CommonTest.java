package hu.bme.onlab.gera.exchangeratemonitor;

import hu.bme.onlab.gera.exchangeratemonitor.accessobjects.FundDataAccessObject;
import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;
import org.junit.Before;

import java.util.*;


public class CommonTest {
    public FundDataAccessObject fdao;
    public List<Fund> testFunds;
    public Date[] excpected_date;

    //Teszt környezet kiépíése
    //két eszközalap:
    //név         dátum      árfolyam
    //-----------------------------
    //Test-alap-1 2012-07-21 1.21
    //Test-alap-1 2012-07-22 -
    //Test-alap-1 2012-07-23 -
    //Test-alap-1 2012-07-24 1.23
    //Test-alap-1 2012-07-25 1.22
    //-----------------------------
    //Test-alap-2 2012-07-21 -
    //Test-alap-2 2012-07-22 -
    //Test-alap-2 2012-07-23 1.4
    //Test-alap-2 2012-07-24 -
    //Test-alap-2 2012-07-25 1.39
    @Before
    public void setUp() throws Exception {
        fdao = new FundDataAccessObject();
        testFunds = new ArrayList<Fund>();
        excpected_date = new Date[2];
        Calendar calendar = new GregorianCalendar();

        //2012-07-21
        calendar.set(Calendar.YEAR,2012);
        calendar.set(Calendar.MONTH,7);
        calendar.set(Calendar.DAY_OF_MONTH,21);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        excpected_date[0] = calendar.getTime();

        Fund temp = new Fund("Test-alap-1");
        temp.putRate(calendar.getTime(), 1.21);

        calendar.add(Calendar.DAY_OF_MONTH,3);
        temp.putRate(calendar.getTime(), 1.23);

        calendar.add(Calendar.DAY_OF_MONTH,1);
        temp.putRate(calendar.getTime(),1.22);

        excpected_date[1] = calendar.getTime();

        testFunds.add(temp);

        //2012-07-23
        calendar.set(Calendar.YEAR,2012);
        calendar.set(Calendar.MONTH,7);
        calendar.set(Calendar.DAY_OF_MONTH,23);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        temp = new Fund("Test-alap-2");
        temp.putRate(calendar.getTime(),1.4);

        calendar.add(Calendar.DAY_OF_MONTH,2);
        temp.putRate(calendar.getTime(),1.39);

        testFunds.add(temp);

    }
}
