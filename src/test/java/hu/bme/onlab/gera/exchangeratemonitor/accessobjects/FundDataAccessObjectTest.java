package hu.bme.onlab.gera.exchangeratemonitor.accessobjects;

import hu.bme.onlab.gera.exchangeratemonitor.CommonTest;
import hu.bme.onlab.gera.exchangeratemonitor.exceptions.NoDataFoundException;
import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

public class FundDataAccessObjectTest extends CommonTest {


    //Ez a teszt demonstrálja a setDate metódus helyes működését
    //2012-07-15 től 2012-07-21 ig nem áll rendelkezésre adat
    //a metódus pedig beállítja 2012-07-21-re
    //ugyanez történik a végdátum esetében is
    @Test
    public void testSetDate_1() throws Exception {
        Calendar from = new GregorianCalendar();
        Calendar to = new GregorianCalendar();

        //2012-07-15
        from.set(Calendar.YEAR, 2012);
        from.set(Calendar.MONTH, 7);
        from.set(Calendar.DAY_OF_MONTH, 15);
        from.set(Calendar.HOUR_OF_DAY, 0);
        from.set(Calendar.MINUTE, 0);
        from.set(Calendar.SECOND, 0);
        from.set(Calendar.MILLISECOND, 0);

        //2012-07-30
        to.set(Calendar.YEAR, 2012);
        to.set(Calendar.MONTH, 7);
        to.set(Calendar.DAY_OF_MONTH, 30);
        to.set(Calendar.HOUR_OF_DAY, 0);
        to.set(Calendar.MINUTE, 0);
        to.set(Calendar.SECOND, 0);
        to.set(Calendar.MILLISECOND, 0);


        fdao.setDate(from,to,testFunds);

        assertEquals(excpected_date[0].getTime(),from.getTime().getTime());
        assertEquals(excpected_date[1].getTime(),to.getTime().getTime());

    }

    //Ebben a tesztben olyan dátum intervallummal dolgozunk, amiben
    //nem áll rendelkezésre adat egyáltalán, ekkor a setDate metódus
    //NoDataFoundException -t dob
    @Test(expected = NoDataFoundException.class)
    public void testSetDate_2() throws NoDataFoundException {
        Calendar from = new GregorianCalendar();
        Calendar to = new GregorianCalendar();

        //2012-07-15
        from.set(Calendar.YEAR, 2012);
        from.set(Calendar.MONTH, 7);
        from.set(Calendar.DAY_OF_MONTH, 15);
        from.set(Calendar.HOUR_OF_DAY, 0);
        from.set(Calendar.MINUTE, 0);
        from.set(Calendar.SECOND, 0);
        from.set(Calendar.MILLISECOND, 0);


        //2012-07-20
        to.set(Calendar.YEAR, 2012);
        to.set(Calendar.MONTH,7);
        to.set(Calendar.DAY_OF_MONTH, 20);
        to.set(Calendar.HOUR_OF_DAY, 0);
        to.set(Calendar.MINUTE, 0);
        to.set(Calendar.SECOND, 0);
        to.set(Calendar.MILLISECOND, 0);


        fdao.setDate(from,to, testFunds);

    }
}
