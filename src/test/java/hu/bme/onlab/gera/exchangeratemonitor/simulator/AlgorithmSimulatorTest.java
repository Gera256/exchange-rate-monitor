package hu.bme.onlab.gera.exchangeratemonitor.simulator;

import hu.bme.onlab.gera.exchangeratemonitor.CommonTest;
import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import static org.junit.Assert.assertEquals;

public class AlgorithmSimulatorTest extends CommonTest {

    //befektetési egységek elhelyezése megadott eszközalapba
    @Test
    public void testPutUnitsInto_1() throws Exception {
        Calendar calendar = new GregorianCalendar();

        //2012-07-23
        calendar.set(Calendar.YEAR,2012);
        calendar.set(Calendar.MONTH,7);
        calendar.set(Calendar.DAY_OF_MONTH,23);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        AlgorithmSimulator as = new AlgorithmSimulator();

        //a megadott eszközalap név : "1" ez a string pedig megtalálható
        //a Test-alap-1 -ben tehát ebbe az eszközalapba teszi a befektetési egységeket
        //Test-alap-1 a listában a 0. helyen szerepel
        as.putUnitsInto(testFunds,calendar.getTime(),"1",3000);

        assertEquals(testFunds.get(0).getUnitCount(),3000);
    }

    //befektetési egységek elhelyezése nem talált/rosszul megadott eszközalap esetén
    @Test
    public void testPutUnitsInto_2() throws Exception {
        Calendar calendar = new GregorianCalendar();

        //2012-07-23
        calendar.set(Calendar.YEAR,2012);
        calendar.set(Calendar.MONTH,7);
        calendar.set(Calendar.DAY_OF_MONTH,23);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        AlgorithmSimulator as = new AlgorithmSimulator();

        //nem létező eszközalap nevű eszközalap nem létezik tehát abba az eszközalapba teszi
        //a befektetési egységeket ahol a legamagasabb az árfolyam érték 2012-07-23 -án
        //ez pedig a Test-alap-2 ami az 1 indexen található a listában
        as.putUnitsInto(testFunds,calendar.getTime(),"nem létező eszközalap",3000);

        assertEquals(testFunds.get(1).getUnitCount(),3000);
    }
}
