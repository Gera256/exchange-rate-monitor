package hu.bme.onlab.gera.exchangeratemonitor.dataobjects;

import hu.bme.onlab.gera.exchangeratemonitor.CommonTest;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

public class FundTest extends CommonTest {

    //Helyes működést demonstráló teszteset
    //a megadott intervallumon belül feltölti az eszközalapok árfolyamadait
    @Test
    public void testFillGaps() throws Exception {
        Date[] testDate = new Date[2];
        Calendar calendar = new GregorianCalendar();

        //2012-07-21
        calendar.set(Calendar.YEAR, 2012);
        calendar.set(Calendar.MONTH, 7);
        calendar.set(Calendar.DAY_OF_MONTH, 21);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        testDate[0] = calendar.getTime();

        //2012-07-25
        calendar.set(Calendar.YEAR, 2012);
        calendar.set(Calendar.MONTH, 7);
        calendar.set(Calendar.DAY_OF_MONTH, 25);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        testDate[1] = calendar.getTime();

        testFunds.get(0).fillGaps(testDate[0],testDate[1]);
        testFunds.get(1).fillGaps(testDate[0],testDate[1]);

        evaluate();

    }

    //Előző teszteset kiértékelése
    //összehasonlítás a várt és kapott eredményekkel
    private void evaluate() {
        Calendar calendar = new GregorianCalendar();

        //2012-07-21
        calendar.set(Calendar.YEAR,2012);
        calendar.set(Calendar.MONTH,7);
        calendar.set(Calendar.DAY_OF_MONTH,21);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        //A Test-alap-1 -ben 2012-07-22 és 2012-07-23 -ára nincs bejegyezve adat viszont
        //2012-07-21 -re van és a fillgaps ezzel az értékkel tölti fel az említett napok adatait
        //ugyanakkor a Test-alap-2 -ben nincs adat 2012-07-23 előttről tehát ezeknek a napoknak az adatait
        //nem is tudja mivel feltölteni
        assertEquals(1.21, testFunds.get(0).getRateByDate(calendar.getTime()), 0.05);

        calendar.add(Calendar.DAY_OF_MONTH,1);
        assertEquals(1.21, testFunds.get(0).getRateByDate(calendar.getTime()), 0.05);

        calendar.add(Calendar.DAY_OF_MONTH, 1);
        assertEquals(1.21, testFunds.get(0).getRateByDate(calendar.getTime()), 0.05);
        assertEquals(1.4, testFunds.get(1).getRateByDate(calendar.getTime()), 0.05);

        calendar.add(Calendar.DAY_OF_MONTH,1);
        assertEquals(1.23,testFunds.get(0).getRateByDate(calendar.getTime()),0.05);
        assertEquals(1.4, testFunds.get(1).getRateByDate(calendar.getTime()), 0.05);

        calendar.add(Calendar.DAY_OF_MONTH,1);
        assertEquals(1.22,testFunds.get(0).getRateByDate(calendar.getTime()),0.05);
        assertEquals(1.39, testFunds.get(1).getRateByDate(calendar.getTime()), 0.05);

    }
}
