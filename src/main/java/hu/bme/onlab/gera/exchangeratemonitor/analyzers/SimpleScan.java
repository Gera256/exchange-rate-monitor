package hu.bme.onlab.gera.exchangeratemonitor.analyzers;

import hu.bme.onlab.gera.exchangeratemonitor.inputdata.ConfigParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by Gera on 2014.05.24..
 */
public class SimpleScan extends Scan {

    SimpleScan(Analyzer analyzer) {
        super(analyzer);
    }

    @Override
    public void prepare(List<ConfigParam> configParams, Map<String, BigDecimal> currentParams) {

        for (ConfigParam cp : configParams){
            currentParams.put(cp.getParamName(),cp.getParamValue());
        }
    }

    @Override
    public boolean scan(Map<String, BigDecimal> currentParams) {
        analyzer.analyze();
        return true;
    }
}
