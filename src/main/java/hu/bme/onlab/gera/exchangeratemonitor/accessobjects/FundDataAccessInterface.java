package hu.bme.onlab.gera.exchangeratemonitor.accessobjects;

import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;
import hu.bme.onlab.gera.exchangeratemonitor.exceptions.NoDataFoundException;

import java.util.Calendar;
import java.util.List;

public interface FundDataAccessInterface {

    public List<Fund> getFundsBetween(Calendar from, Calendar to) throws NoDataFoundException;
}
