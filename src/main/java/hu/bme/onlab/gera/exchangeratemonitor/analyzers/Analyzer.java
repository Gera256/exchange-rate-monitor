package hu.bme.onlab.gera.exchangeratemonitor.analyzers;

import hu.bme.onlab.gera.exchangeratemonitor.accessobjects.FundDataAccessInterface;
import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;
import hu.bme.onlab.gera.exchangeratemonitor.exceptions.NoDataFoundException;
import hu.bme.onlab.gera.exchangeratemonitor.inputdata.ConfigParam;
import hu.bme.onlab.gera.exchangeratemonitor.inputdata.SimulatorInput;
import hu.bme.onlab.gera.exchangeratemonitor.simulator.AlgorithmSimulator;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Gera on 2014.05.13..
 */
public class Analyzer {

    FundDataAccessInterface fundDao;
    AlgorithmSimulator test;
    SimulatorInput input;
    List<ConfigParam> configParams;
    Scan scanMode;
    //Adatbázisból betöltött és szűrt adatok ebben a listában tárolódnak
    List<Fund> allData = new ArrayList<Fund>();
    Map<String,BigDecimal> currentParams = new HashMap<String, BigDecimal>();
    PrintWriter pw = null;
    Fund end = null;

    public Analyzer(AlgorithmSimulator test, SimulatorInput input, List<ConfigParam> configParams, FundDataAccessInterface fundDao){
        this.test = test;
        this.input = input;
        this.configParams = configParams;
        this.fundDao = fundDao;
    }

    public  void setScanMode(Scan scanMode){
        this.scanMode = scanMode;
    }

    public void start() throws IOException {

        Calendar from = new GregorianCalendar();
        Calendar to = new GregorianCalendar();

        from.setTime(input.getFrom());
        to.setTime(input.getTo());

        //adatok betöltése.
        //amennyiben a megadott dátum intervallumon belül nincs árfolyamadat
        //NoDataFoundException-t kapunk, mivel így nincs értelme a program további futásának
        //ezt kilépéssel kezelem
        try {
            allData = fundDao.getFundsBetween(from,to);
        } catch (NoDataFoundException e) {
            e.printStackTrace();
            System.exit(2);
        }

        input.setFrom(from.getTime());
        input.setTo(to.getTime());
        scanMode.prepare(configParams, currentParams);

        pw = new PrintWriter(new FileWriter("hozam.txt"));

        scanMode.scan(currentParams);

    }

    public void analyze(){

            try {
                pw.println("--------------------------------------");
                pw.println("unsafeFundToleranceBand: " + currentParams.get("unsafeFundToleranceBand").floatValue());
                pw.println("unsafeFundToleranceBand2: " + currentParams.get("unsafeFundToleranceBand2").floatValue());
                pw.println("--------------------------------------");
                System.out.println("--------------------------------------");
                System.out.println("unsafeFundToleranceBand: " + currentParams.get("unsafeFundToleranceBand").floatValue());
                System.out.println("unsafeFundToleranceBand2: " + currentParams.get("unsafeFundToleranceBand2").floatValue());
                System.out.println("--------------------------------------");
                end = test.simulate(input,currentParams,allData);
                pw.println(end.getValue(end.lastDate()).toString());
                pw.flush();
                System.out.println(end.getValue(end.lastDate()));
            } catch (IOException e) {
                e.printStackTrace();
            }

    }
}
