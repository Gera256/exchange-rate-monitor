package hu.bme.onlab.gera.exchangeratemonitor.algorithms;

import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Gera on 2014.05.12..
 */
public class UnsafeFundStrategy extends FundStrategy {
    @Override
    public void setPotentialFunds(Fund current, List<Fund> funds, Map<String, BigDecimal> config, Date today) {
        int fundTypeSeparator = config.get("fundTypeSeparator").intValue();
        int movingAvgPeriod = config.get("movingAvgPeriod").intValue();
        float unsafeFundToleranceBand = config.get("unsafeFundToleranceBand2").floatValue();

        if(change(current,today,movingAvgPeriod,unsafeFundToleranceBand)){
            for (Fund f : funds){
                if (f.getStability() < fundTypeSeparator)
                    funds.remove(f);
            }
        }
        else{
            for (Fund f : funds){
                funds.remove(f);
            }
        }

    }

    private boolean change(Fund current, Date today, int movingAvgPeriod, float unsafeFundToleranceBand){
        Calendar calendar = new GregorianCalendar();
        double movingAvg = 0;
        double lowerThreshold = 0;
        calendar.setTime(today);
        calendar.add(Calendar.DAY_OF_MONTH, -1* movingAvgPeriod);

        if(!current.hasValueAt(calendar.getTime()))
            return false;
        else{
            movingAvg = getMovingAvgValue(current, calendar.getTime(), movingAvgPeriod);
            lowerThreshold = movingAvg*(1+unsafeFundToleranceBand);
            calendar.setTime(today);
            calendar.add(Calendar.DAY_OF_MONTH,-1);

            return current.getRateByDate(calendar.getTime()) <= lowerThreshold;
        }
    }

    private double getMovingAvgValue(Fund f, Date startDate, int movingAvgPeriod) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);
        double sum = 0;

        for(int i = 0; i <= movingAvgPeriod; i++){
            sum += f.getRateByDate(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH,1);
        }

        return (sum/movingAvgPeriod);
    }


}
