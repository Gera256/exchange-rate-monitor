package hu.bme.onlab.gera.exchangeratemonitor.analyzers;

import hu.bme.onlab.gera.exchangeratemonitor.inputdata.ConfigParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by Gera on 2014.05.14..
 */
public class FullScan extends Scan {

    FullScan(Analyzer analyzer) {
        super(analyzer);
    }

    @Override
    public void prepare(List<ConfigParam> configParams, Map<String, BigDecimal> currentParams) {

    }

    @Override
    public boolean scan(Map<String, BigDecimal> currentParams) {
        return false;
    }
}
