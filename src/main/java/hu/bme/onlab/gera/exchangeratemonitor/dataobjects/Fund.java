package hu.bme.onlab.gera.exchangeratemonitor.dataobjects;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

/**
 * Az eszközalapokat reprezentáló osztály
 */
@Entity
public class Fund {

    @Id
    @GeneratedValue
    private long id;

    private String name;

    //rate adatbázistábla leképezése egy rendezett map-be
    @ElementCollection
    @CollectionTable(name = "rate")
    @MapKeyColumn(name = "rate_date")
    @MapKeyTemporal(TemporalType.DATE)
    @Column(name = "rate_value")
    @Sort(type = SortType.NATURAL)
    @Fetch(FetchMode.JOIN)
    private SortedMap<Date, Double> rates;

    private int stability;
    @Transient
    private int unitCount;

    //Hibernate-nek
    public Fund(){

    }

    //Teszteléshez
    public Fund(String name) {
        this.name = name;
        rates = new TreeMap<Date,Double>();
        stability = 0;
        unitCount = 0;
    }

    //Copy constructor szerűség, az "A" feladatban szereplő algoritmus nem közvetlen az adatbázisból
    //szedett adatokon dolgozik, hanem azok másolatán. Bővebb leírás az AlgorithmSimulator osztályban található.
    public Fund(Fund f){
        this.id = f.id;
        this.name = f.name;
        this.rates = new TreeMap<Date,Double>();
        this.stability = f.stability;
        this.unitCount = f.unitCount;
    }

    //Az adatbázisból szedett adatok hiányosak, olyan értelemben, hogy előfordulnak dátum értékek,
    //melyekre nincs bejegyezve árfolyamadat de például előtte meg utána pár nappal már van. Ez a metódus az
    //ilyen lukakat foltozza be, oly módon, hogy tartja az előző értéket.
    public void fillGaps(Date from, Date to) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(from);
        //Ez a temp változó tárolja a legutolsó árfolyamértéket.
        double temp = 0;
        if (hasValueAt(calendar.getTime())){
            temp = rates.get(calendar.getTime());
        }
        calendar.add(Calendar.DAY_OF_MONTH, 1);

        while (calendar.getTime().compareTo(to) < 0){
            if (temp > 0 && rates.get(calendar.getTime()) == null){
                rates.put(calendar.getTime(),temp);
            }

            if (hasValueAt(calendar.getTime())){
                temp = rates.get(calendar.getTime());
            }
            calendar.add(Calendar.DAY_OF_MONTH,1);
        }
    }

    //Ez a metódus a paraméterként kapott dátum értékeken kívül eső adatokat kitörli.
    public void truncate(Date from, Date to){
        Calendar toCal = new GregorianCalendar();
        toCal.setTime(to);

        toCal.add(Calendar.DAY_OF_MONTH,1);
        rates = rates.subMap(from,toCal.getTime());
    }

    //Árfolyam értéket rögzít az adott dátumra.
    public void putRate(Date time, double value){
        rates.put(time,value);
    }

    //Ez a metódus visszaadja a befektetés értékét.
    public BigDecimal getValue(Date time) {
        BigDecimal value = new BigDecimal(rates.get(time));
        value = value.multiply(new BigDecimal(unitCount));
        return value;
    }

    public double getRateByDate(Date time) {
            return rates.get(time);
    }

    public boolean hasValueAt(Date time) {
        return (rates.get(time) != null);
    }

    public void setUnitCount(int unitCount) {
        this.unitCount = unitCount;
    }

    public int getUnitCount() {
        return unitCount;
    }

    public String getName(){
        return name;
    }

    public int getStability(){
        return stability;
    }

    public Date lastDate(){
        return rates.lastKey();
    }

}
