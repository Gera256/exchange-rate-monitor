package hu.bme.onlab.gera.exchangeratemonitor.inputdata;

import java.math.BigDecimal;

/**
 * Az "A" feladatban szereplő algoritmus számára átadott paramétereket reprezentálja.
 */
public class ConfigParam {
    private String paramName;
    private BigDecimal paramValue;
    private BigDecimal minValue;
    private BigDecimal maxValue;
    private BigDecimal stepValue;

    public ConfigParam(String name, float value, float minValue, float maxValue, float stepValue){
        paramName = name;
        paramValue = new BigDecimal(value);
        this.minValue = new BigDecimal(minValue);
        this.maxValue = new BigDecimal(maxValue);
        this.stepValue = new BigDecimal(stepValue);
    }

    public boolean increment(){
        if (paramValue.add(stepValue).compareTo(maxValue) <= 0){
            paramValue = paramValue.add(stepValue);
            return true;
        }
        else
            return false;
    }

    public String getParamName() {
        return paramName;
    }

    public BigDecimal getParamValue(){
        return paramValue;
    }

    public void setParamValue(BigDecimal val){
        paramValue = val;
    }

    public BigDecimal getMinValue() {
        return minValue;
    }

    public BigDecimal getStepValue() {
        return stepValue;
    }
}
