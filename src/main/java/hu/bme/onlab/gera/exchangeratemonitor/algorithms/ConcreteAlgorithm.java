package hu.bme.onlab.gera.exchangeratemonitor.algorithms;

import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class ConcreteAlgorithm implements Algorithm {

    private Data data;
    private FundStrategy strategy;

    @Override
    public Fund calculate(List<Fund> funds, Map<String,BigDecimal> config, Date today) {
        Fund destFund = null;
        Fund actualFund = null;
        List<Fund> potentialFunds = new CopyOnWriteArrayList<Fund>();
        int fundTypeSeparator = config.get("fundTypeSeparator").intValue();
        int numberOfPunishmentDays = config.get("numberOfPunishmentDays").intValue();

        loadData(funds);

        for (Fund f : funds){
            if(data.punishmentTable.get(f.getName()) == null)
                data.punishmentTable.put(f.getName(),0);
            if(f.getUnitCount() !=0)
                actualFund = f;
            else
                potentialFunds.add(f);
        }

        if(actualFund.getStability() > fundTypeSeparator)
            strategy = new SafeFundStrategy();
        else
            strategy = new UnsafeFundStrategy();

        strategy.setPotentialFunds(actualFund,potentialFunds,config,today);

        data.decrease();

        if(!potentialFunds.isEmpty()){
            destFund = potentialFunds.get(0);
            if (data.punishmentTable.get(destFund.getName()) == 0){
                changeFund(actualFund,destFund,today);
                if (actualFund.getStability() < fundTypeSeparator && destFund.getStability() < fundTypeSeparator)
                    data.punishmentTable.put(actualFund.getName(),2*numberOfPunishmentDays);
                else if (actualFund.getStability() < fundTypeSeparator && destFund.getStability() > fundTypeSeparator)
                    data.punishmentTable.put(actualFund.getName(),4*numberOfPunishmentDays);
            }
            else
                destFund = null;
        }

        saveData();

        return destFund;
    }

    @Override
    public void reset() {
        data = null;
        File f = new File(System.getProperty("user.dir")+ "/data.ser");
        f.delete();
    }

    private void saveData() {
        try {
            FileOutputStream fileIO = new FileOutputStream("data.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileIO);
            out.writeObject(data);
            out.close();

        }  catch (IOException e) {
            e.printStackTrace();
        }

    }

    void changeFund(Fund actualFund, Fund destFund, Date today){
        BigDecimal value = actualFund.getValue(today);
        value = value.divide(new BigDecimal(destFund.getRateByDate(today)),10, RoundingMode.HALF_UP);
        destFund.setUnitCount(value.intValue());
        actualFund.setUnitCount(0);
    }

    void loadData(List<Fund> funds) {
        try {
            FileInputStream fileIO = new FileInputStream("data.ser");
            ObjectInputStream in = new ObjectInputStream(fileIO);
            data = (Data)in.readObject();
            in.close();

        } catch (FileNotFoundException e) {
            data = new Data();
            for (Fund f : funds){
                data.punishmentTable.put(f.getName(),0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
}
