package hu.bme.onlab.gera.exchangeratemonitor.simulator;

import hu.bme.onlab.gera.exchangeratemonitor.algorithms.Algorithm;
import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;
import hu.bme.onlab.gera.exchangeratemonitor.inputdata.SimulatorInput;
import au.com.bytecode.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class AlgorithmSimulator
{
    Algorithm alg;
    Calendar from = new GregorianCalendar();
    Calendar to = new GregorianCalendar();

    //Teszteléshez
    public AlgorithmSimulator(){

    }

    public AlgorithmSimulator(Algorithm alg){
        this.alg = alg;
    }

    //Az "A" feladatban szereplő algoritmus futtatása a megadott dátum intervallumon belül
    public Fund simulate(SimulatorInput input, Map<String,BigDecimal> params, List<Fund> allData) throws IOException {

        //bemeneti adatok változóba mentése
        from.setTime(input.getFrom());
        to.setTime(input.getTo());

        //az "A" feladatban szereplő algoritmus ebben a listában tárolt adatokkal dolgozik
        List<Fund> algData = new ArrayList<Fund>();

        //fájlba írásnál a dátumokat ennek az objektumnak a segítségével formázom
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //fájlba írásnál a befektetés értékét ennek az objektumnak a segítségével formázom
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
        DecimalFormat decimalFormat = (DecimalFormat)numberFormat;
        decimalFormat.applyPattern("#.##");

        //a kimeneti adatokat két .csv fájlba írom ki, az ehhez használt segédosztályok
        //inicializálása itt valósul meg
        CSVWriter investedUnits = new CSVWriter(new FileWriter("investedUnits.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER, "\r\n");
        CSVWriter changes = new CSVWriter(new FileWriter("changes.csv"), ',', CSVWriter.NO_QUOTE_CHARACTER, "\r\n");
        String[] IULine = "eszkozalap neve,egyseg(db),befektetes erteke".split(",");
        String[] CLine = "datum,forras eszkozalap,cel eszkozalap".split(",");

        //temporális változók
        //azt az eszközalapot tárolja amelyikbe éppen a befektetési
        //egységeink vannak
        Fund actualFund = null;
        //ha az "A" feladatban szereplő algoritmus átváltást javasol akkor
        //az átváltás utáni eszközalap ebben a változóban tárolódik
        Fund returnValue = null;
        //befektetési egységek darabszámát tároló változó
        int unit;
        //befektetés értékét tároló változó
        BigDecimal value;

        //az első nap adataival feltöltjük az algData listát,
        //hogy legyen objektum amibe tárolni tudjuk a befektetett egységeket
        buildData(allData,algData,from.getTime());

        //befektetési egységek eszközalapba helyezése
        //az actualFund változó tárolja, hogy éppen melyikbe vannak.
        actualFund = putUnitsInto(algData, from.getTime(), input.getFundName(),input.getUnitCount());

        //referencia dátum növelése
        from.add(Calendar.DAY_OF_MONTH, 1);

        //.csv fájl fejléceinek kiírása
        investedUnits.writeNext(IULine);
        changes.writeNext(CLine);

        //az "A" feladatban szereplő algoritmus futtatása a megadott intervallumon belül
        //és kimenetének fájlba írása
        while (from.compareTo(to) <= 0){
            //az "A" feladatban szereplő algoritmus külön adaton (algData)
            buildData(allData,algData,from.getTime());
            //az "A" feladatban szereplő algoritmus futtatása
            returnValue = alg.calculate(algData,params,from.getTime());
            //visszatérési érték alapján kiírunk fájlba
            if (returnValue == null){
                unit = actualFund.getUnitCount();
                value = actualFund.getValue(from.getTime());

                //fájlba írás
                investedUnits.writeNext(new String[] {actualFund.getName(), String.valueOf(unit), decimalFormat.format(value)});
            }
            else{
                //fájlba írás
                changes.writeNext(new String[] {dateFormat.format(from.getTime()),actualFund.getName(),returnValue.getName()});
                actualFund = returnValue;
                unit = actualFund.getUnitCount();
                value = actualFund.getValue(from.getTime());
                //fájlba írás
                investedUnits.writeNext(new String[] {actualFund.getName(), String.valueOf(unit), decimalFormat.format(value)});
            }
            //referencia dátum növelése
            from.add(Calendar.DAY_OF_MONTH, 1);
        }

        //erőforrások felszabadítása
        investedUnits.close();
        changes.close();

        //az algoritmus adatainak törlése
        alg.reset();

        return actualFund;
    }

    //Ez a metódus a megadott nevű eszközalapba helyezi a befektetési egységeket, a megadott napra
    //ha nem találja meg a megadott nevű eszközalapot akkor az adott napon rendelkezésre álló
    //eszközalapok közül kiválasztja azt, amelyiknek az adott napon a legmagasabb az árfolyama
    Fund putUnitsInto(List<Fund> funds, Date from, String fundName, int unitCount) {
        Fund temp = null;
        double max = 0;
        int index = 0;

        //keresés név alapján
        index = searchFundByName(fundName,funds);

        //találat esetén beállítjuk a befektetési egységeket a megfelelő
        //eszközalap objektumban
        if (index != -1)
            funds.get(index).setUnitCount(unitCount);

        //ellenkező esetben megkeressük, hogy az adott napon melyik eszközalap
        //árfolyama a legmagasabb
        else {
            for (Fund f : funds){
                if(f.hasValueAt(from)){
                    if (f.getRateByDate(from) > max ){
                        temp = f;
                        max = f.getRateByDate(from);
                    }
                }
            }
            //a temp értéke semmiképpen nem marad null ha a program futása eljut idáig
            index = searchFundByName(temp.getName(),funds);
            funds.get(index).setUnitCount(unitCount);
        }

        return funds.get(index);
    }

    //Ez a metódus a második paraméterként kapott listát bővíti árfolyamadatokkal (esetleg új eszközalappal)
    //Cél: adott nap árfolyamadatait áttölteni az algData listába
    //végigmegyünk a forráslistán ami az adatbázisból betöltött adatokat tartalmazza
    //ha az adott napon az adott eszközalaphoz tartozik árfolyamadat akkor
    //áttöltjük az algData listába, ha ebbe a listába már benne van az adott eszközalap
    //akkor egyszrűen csak hozzáadjuk az árfolyamadatot, ellenkező esetben létrehozunk egy új eszközalap
    //objektumot és azt töltjük fel az árfolyamadattal és ezt adjuk hozzá a listához
    void buildData(List<Fund> sourceList, List<Fund> algData, Date time) {
        for (Fund fund : sourceList){
            if (fund.hasValueAt(time)){
                int index = searchFundByName(fund.getName(),algData);
                double rateValue = fund.getRateByDate(time);
                if (index > -1){
                    algData.get(index).putRate(time, rateValue);
                }
                else {
                    Fund temp = new Fund(fund);
                    algData.add(temp);
                    algData.get(algData.size()-1).putRate(time, rateValue);
                }
            }
        }
    }

    //Ez a metódus visszaadja, hogy hányadik helyen található az adott nevű eszközalap
    // (elég rész név is) találat hiányában -1 el tér vissza.
    int searchFundByName(String name,List<Fund> funds) {
        for (Fund f : funds){
            if (f.getName().contains(name))
                return funds.indexOf(f);
        }
        return -1;
    }
}
