package hu.bme.onlab.gera.exchangeratemonitor.analyzers;

import hu.bme.onlab.gera.exchangeratemonitor.inputdata.ConfigParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by Gera on 2014.05.14..
 */
public abstract class Scan {
    Analyzer analyzer;

    Scan(Analyzer analyzer){
        this.analyzer = analyzer;
    }

    public abstract void prepare(List<ConfigParam> configParams, Map<String, BigDecimal> currentParams);
    public abstract boolean scan(Map<String, BigDecimal> currentParams);
}
