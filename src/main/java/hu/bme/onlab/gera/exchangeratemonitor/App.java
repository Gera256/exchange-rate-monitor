package hu.bme.onlab.gera.exchangeratemonitor;

import hu.bme.onlab.gera.exchangeratemonitor.analyzers.Analyzer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * A "B" feladatot megvalósító osztályt futtatja.
 */
public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/AnalyzerContext.xml");
        Analyzer a = (Analyzer)context.getBean("Analyzer");
        try {
            a.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
