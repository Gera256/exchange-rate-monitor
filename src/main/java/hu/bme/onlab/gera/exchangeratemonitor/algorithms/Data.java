package hu.bme.onlab.gera.exchangeratemonitor.algorithms;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Gera on 2014.05.11..
 */
public class Data implements Serializable {
    HashMap<String,Integer> punishmentTable;

    Data(){
        punishmentTable = new HashMap<String,Integer>();
    }

    public void decrease() {
        Iterator<Map.Entry<String,Integer>> iter = punishmentTable.entrySet().iterator();
        while(iter.hasNext()){
            Map.Entry<String,Integer> current = iter.next();
            if (current.getValue() > 0)
                current.setValue(current.getValue() - 1);
        }
    }
}
