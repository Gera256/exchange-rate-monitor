package hu.bme.onlab.gera.exchangeratemonitor.analyzers;

import hu.bme.onlab.gera.exchangeratemonitor.inputdata.ConfigParam;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Gera on 2014.05.14..
 */
public class ToleranceBandRangeScan extends Scan {
    List<ConfigParam> usedParams;

    ToleranceBandRangeScan(Analyzer analyzer) {
        super(analyzer);
    }

    public void prepare(List<ConfigParam> configParams, Map<String, BigDecimal> currentParams){
        usedParams = new ArrayList<ConfigParam>();

        for (ConfigParam cp : configParams){
            if (cp.getParamName().equals("unsafeFundToleranceBand"))
                usedParams.add(cp);
            else if (cp.getParamName().equals("unsafeFundToleranceBand2"))
                usedParams.add(cp);
        }

        for (ConfigParam cp : usedParams){
            cp.setParamValue(cp.getMinValue().subtract(cp.getStepValue()));
        }

    }

    @Override
    public boolean scan(Map<String, BigDecimal> currentParams) {

        while (usedParams.get(0).increment()){
            while (usedParams.get(1).increment()){
                setParams(currentParams);
                analyzer.analyze();
            }
            usedParams.get(1).setParamValue(usedParams.get(1).getMinValue().subtract(usedParams.get(1).getStepValue()));
        }

        return true;
    }

    private void setParams(Map<String, BigDecimal> currentParams) {
        for (ConfigParam cp : usedParams){
            currentParams.put(cp.getParamName(),cp.getParamValue());
        }
    }
}
