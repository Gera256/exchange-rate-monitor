package hu.bme.onlab.gera.exchangeratemonitor.algorithms;

import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface Algorithm {
    public Fund calculate(List<Fund> data, Map<String,BigDecimal> config, Date today);
    public void reset();
}
