package hu.bme.onlab.gera.exchangeratemonitor.algorithms;

import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Gera on 2014.05.12..
 */
public abstract class FundStrategy {

    public abstract void setPotentialFunds(Fund current, List<Fund> funds, Map<String,BigDecimal> config, Date today);
}
