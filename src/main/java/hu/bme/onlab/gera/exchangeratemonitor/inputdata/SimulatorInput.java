package hu.bme.onlab.gera.exchangeratemonitor.inputdata;

import java.util.Date;
/**
 * A "B" feladatban szereplő tesztelő bemeneteit tartalmazó osztály
 * Változók: Kezdeti dátum, végdátum, a kiindulási eszközalap neve, befektetési egységek darabszáma
 */
public class SimulatorInput {
    private Date from;
    private Date to;
    private String fundName;
    private int unitCount;

    public SimulatorInput(Date from, Date to, String fundName, int unitCount){
        this.from = from;
        this.to = to;
        this.fundName = fundName;
        this.unitCount =  unitCount;

        checkInput();
    }

    //bemeneti adatok ellenőrzése
    public void checkInput(){
        if (from.after(to)){
            System.out.println("A kezdeti dátum később van mint a befejezés dátuma.");
            System.exit(0);
        }

        if (unitCount < 1){
            System.out.println("A befektetési egységek darabszáma nem éri el a minimum szintet (1 egység).");
            System.exit(1);
        }
    }

    public Date getFrom() {
        return from;
    }

    public Date getTo() {
        return to;
    }

    public void setFrom(Date from){
        this.from = from;
    }

    public void setTo(Date To){
        this.to = To;
    }

    public String getFundName() {
        return fundName;
    }

    public int getUnitCount() {
        return unitCount;
    }
}
