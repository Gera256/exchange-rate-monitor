package hu.bme.onlab.gera.exchangeratemonitor.algorithms;

import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by Gera on 2014.05.12..
 */
public class SafeFundStrategy extends FundStrategy{

    @Override
    public void setPotentialFunds(Fund current, List<Fund> funds , Map<String,BigDecimal> config, Date today){
        int movingAvgPeriod = config.get("movingAvgPeriod").intValue();
        float unsafeFundToleranceBand = config.get("unsafeFundToleranceBand").floatValue();
        Calendar calendar = new GregorianCalendar();

        for (Fund f : funds){
            if (!isPotential(f,today,movingAvgPeriod,unsafeFundToleranceBand))
                funds.remove(f);
        }

        double diffrate;
        double avgDiffrate = 0.0005;
        Fund best = null;

        for (Fund f : funds){
            calendar.setTime(today);
            diffrate = f.getRateByDate(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, -1* movingAvgPeriod);
            diffrate -= getMovingAvgValue(f, calendar.getTime(), movingAvgPeriod);
            if (avgDiffrate < diffrate/movingAvgPeriod){
                avgDiffrate = diffrate/movingAvgPeriod;
                best = f;
            }
        }

        if (funds.indexOf(best) != -1)
            Collections.swap(funds,funds.indexOf(best),0);
    }

    private boolean isPotential(Fund f, Date today, int movingAvgPeriod, float toleranceBand) {
        Calendar calendar = new GregorianCalendar();
        double upperThreshold = 0;
        double movingAvg = 0;
        calendar.setTime(today);
        calendar.add(Calendar.DAY_OF_MONTH, -1* movingAvgPeriod);

        if(!f.hasValueAt(calendar.getTime()))
            return false;
        else{
            movingAvg = getMovingAvgValue(f, calendar.getTime(), movingAvgPeriod);
            upperThreshold = movingAvg*(1+toleranceBand);
            calendar.setTime(today);
            calendar.add(Calendar.DAY_OF_MONTH,-1);

            return f.getRateByDate(calendar.getTime()) >= upperThreshold;
        }
    }

    private double getMovingAvgValue(Fund f, Date startDate, int movingAvgPeriod) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);
        double sum = 0;

        for(int i = 0; i <= movingAvgPeriod; i++){
            sum += f.getRateByDate(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH,1);
        }

        return (sum/movingAvgPeriod);
    }
}
