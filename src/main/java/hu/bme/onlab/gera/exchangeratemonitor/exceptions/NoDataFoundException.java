package hu.bme.onlab.gera.exchangeratemonitor.exceptions;

public class NoDataFoundException extends Exception {
    public NoDataFoundException(){
        super();
    }
    public NoDataFoundException(String msg){
        super(msg);
    }
}
