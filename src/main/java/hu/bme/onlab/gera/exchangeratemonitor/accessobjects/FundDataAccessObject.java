package hu.bme.onlab.gera.exchangeratemonitor.accessobjects;

import hu.bme.onlab.gera.exchangeratemonitor.dataobjects.Fund;
import hu.bme.onlab.gera.exchangeratemonitor.exceptions.NoDataFoundException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A minta adatokat tartalmazó osztály
 */
public class FundDataAccessObject implements FundDataAccessInterface {

    private SessionFactory sessionFactory;

    //Teszteléshez
    public FundDataAccessObject() {

    }

    public FundDataAccessObject(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    //Ez a metódus felelős azért, hogy az adatbázisból visszaadja a megfelelő adatokat
    //pontosabban, az adatbázisból kiolvassa az összes adatot ( eszközalapot és a hozzátartozó
    //árfolyamadatot) , majd ezt leszűri különböző segédmetódusokkal.
    //Megjegyzés: az object-relational mapping nem igazán támogatja a bonyolult lekérdezéseket
    //ezért nem tudtam az adatbázisból eleve a szűrt adatokat kiolvasni.
    @Override
    public List<Fund> getFundsBetween(Calendar from, Calendar to) throws NoDataFoundException {
        Transaction tx = sessionFactory.getCurrentSession().beginTransaction();
        List<Fund> funds = sessionFactory.getCurrentSession().createQuery("from Fund f").list();
        tx.commit();

        setDate(from,to,funds);

        for (Fund f : funds){
            f.truncate(from.getTime(),to.getTime());
            f.fillGaps(from.getTime(),to.getTime());
        }

        return funds;
    }

    //Ez a metódus nem módosítja a paraméterként kapott eszközalap adatokat
    //pusztán a dátum értékeket az adatokhoz igazítja, oly módon hogyha a
    //paraméterként kapott napon nem áll rendelkezésre árfolyamadat akkor
    //szűkíti az intervallumot, ha pedig egyáltalán nincs adat a két megadott dátum között
    //akkor NoDataFoundException-t dob
    void setDate(Calendar fromCal, Calendar toCal, List<Fund> list) throws NoDataFoundException {

        boolean fromOK = fundsHasValueAt(fromCal.getTime(),list);
        boolean toOK = fundsHasValueAt(toCal.getTime(),list);

        while (fromCal.compareTo(toCal) < 0 && (!fromOK || !toOK)){
            if(!fromOK){
                fromCal.add(Calendar.DAY_OF_MONTH,1);
                fromOK = fundsHasValueAt(fromCal.getTime(),list);
            }

            if(!toOK){
                toCal.add(Calendar.DAY_OF_MONTH,-1);
                toOK = fundsHasValueAt(toCal.getTime(),list);
            }
        }

        if(fromCal.getTime().getTime() >= toCal.getTime().getTime()){
            throw new NoDataFoundException();
        }
    }


    //Ez a metódus igaz értékkel tér vissza akkor, ha a megadott napra
    //van árfolyam adat legalább egy eszközalaphoz
    //ellenkező esetben hamis értékkel tér vissza
    boolean fundsHasValueAt(Date when,List<Fund> list){
        boolean hasValue = false;
        for(Fund f : list){
            if (f.hasValueAt(when)){
                hasValue = true;
                break;
            }
        }

        return hasValue;
    }
}
